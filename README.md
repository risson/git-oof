# git out of office (git-oof)

Working on public projects during your office time? Trying to make your commits
look like they were made during your off time? Then this is for you!

`git-oof` will change your commit times to make it appear as you made them
during your non-working hours.

### Installation

```
cargo install git-oof
```

### Configuration

Shown with the default values:

```sh
# The start of your workday
git config oof.start 09:00
# The end of your workday
git config oof.end 18:00
# The days during which git-oof must be active
git config oof.days Mon,Tue,Wed,Thu,Fri
```

### Usage

```
git oof <your git command>
```

It just acts as a proxy for your other git commands and aliases, so just prefix
them with `oof` and you're good to go.

### Inner workings

`git-oof` will shrink a day to make your commits fit inside your non-working
hours. For example, if you work from 00:00 to 12:00, and make a commit at
03:00, this commit will have a time of 13:30.
