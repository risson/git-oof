{
  description = "A git command to change your commit time to make it appear as you committed on your personal time.";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    rust-overlay.url = "github:oxalica/rust-overlay";
    futils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, rust-overlay, futils } @ inputs:
    let
      inherit (nixpkgs) lib;
      inherit (lib) recursiveUpdate;
      inherit (futils.lib) eachDefaultSystem defaultSystems;

      nixpkgsFor = lib.genAttrs defaultSystems (system: import nixpkgs {
        inherit system;
        overlays = [
          rust-overlay.overlay
          self.overlay
        ];
      });

      anySystemOutputs = {
        overlay = final: prev: {
          git-oof = final.rustPlatform.buildRustPackage {
            pname = "git-oof";
            version = "0.1.0";

            src = self;

            cargoSha256 = "sha256-IZT751NHxTw+0mzeS4Z0/WMZvCHuz3VQERU4rCHNQfE=";

            meta = with lib; {
              homepage = "https://gitlab.com/risson/git-oof";
              description = "A git command to change your commit time to make it appear as you committed on your personal time.";
              license = licenses.mit;
              maintainers = with maintainers; [ risson ];
            };
          };
        };
      };

      multipleSystemsOutputs = eachDefaultSystem (system:
        let
          pkgs = nixpkgsFor.${system};
        in
        {
          devShell = pkgs.mkShell {
            buildInputs = with pkgs; [
              rust-bin.stable."1.58.1".default
              cargo-audit
              cargo-edit
              cargo-expand
              cargo-flamegraph
              cargo-release
              cargo-sort
              cargo-tarpaulin
              git
              pre-commit
            ];

            RUST_SRC_PATH = "${pkgs.rust.packages.stable.rustPlatform.rustLibSrc}";
          };

          packages = {
            inherit (pkgs) git-oof;
          };
          defaultPackage = self.packages.${system}.git-oof;

          apps = {
            git-oof = {
              type = "app";
              program = "${self.defaultPackage.${system}}/bin/git-oof";
            };
          };
          defaultApp = self.apps.${system}.git-oof;
        });
    in
    recursiveUpdate multipleSystemsOutputs anySystemOutputs;
}
