use anyhow::{anyhow, Result};
use chrono::{DateTime, Duration, Local, NaiveDateTime, NaiveTime, TimeZone};
use git2::Repository;
use std::collections::HashMap;
use std::env;
use std::process::Command;

fn get_config_option(config: &git2::Config, name: &str) -> Result<String> {
    match config.get_string(name) {
        Ok(value) => Ok(value),
        Err(err)
            if err.class() == git2::ErrorClass::Config
                && err.code() == git2::ErrorCode::NotFound =>
        {
            Err(anyhow!("Config option {} not found", name))
        }
        Err(err) => Err(anyhow!(err)),
    }
}

fn main() -> Result<()> {
    let repo = Repository::discover(env::current_dir()?)?;
    let config = repo.config()?.snapshot()?;

    let start = get_config_option(&config, "oof.start").unwrap_or_else(|_| "09:00".to_string());
    let start = NaiveTime::parse_from_str(&start, "%H:%M")?;
    let end = get_config_option(&config, "oof.end").unwrap_or_else(|_| "18:00".to_string());
    let end = NaiveTime::parse_from_str(&end, "%H:%M")?;

    let days = get_config_option(&config, "oof.days")
        .unwrap_or_else(|_| "Mon,Tue,Wed,Thu,Fri".to_string());

    let now = Local::now().naive_local();

    let previous_start = NaiveDateTime::new(now.date(), start);
    let previous_start = if now < previous_start {
        NaiveDateTime::new(now.date() - Duration::days(1), start)
    } else {
        previous_start
    };

    let previous_end = NaiveDateTime::new(previous_start.date(), end);
    let previous_end = if previous_end < previous_start {
        NaiveDateTime::new(previous_start.date() + Duration::days(1), end)
    } else {
        previous_end
    };

    let now: DateTime<Local> = Local.from_local_datetime(&now).unwrap();
    let previous_start: DateTime<Local> = Local.from_local_datetime(&previous_start).unwrap();
    let previous_end: DateTime<Local> = Local.from_local_datetime(&previous_end).unwrap();

    let next_start = previous_start + Duration::days(1);

    let oof_duration = Duration::days(1) - (previous_end - previous_start);

    let current_offset = next_start - now;
    let target_offset =
        current_offset.num_seconds() * oof_duration.num_seconds() / Duration::days(1).num_seconds();
    let target_offset = Duration::seconds(target_offset);

    let target_time = previous_start + target_offset;
    let target_time_formatted = target_time.to_rfc3339();

    let mut envs = HashMap::new();

    let previous_start_weekday = previous_start.format("%a").to_string();
    let previous_end_weekday = previous_end.format("%a").to_string();
    if days
        .split(',')
        .any(|day| day == previous_start_weekday || day == previous_end_weekday)
    {
        envs.insert("GIT_AUTHOR_DATE", &target_time_formatted);
        envs.insert("GIT_COMMITTER_DATE", &target_time_formatted);
    }

    let status = Command::new("git")
        .args::<Vec<String>, String>(env::args().skip(1).collect())
        .envs(envs)
        .status()?;

    std::process::exit(status.code().unwrap_or(0));
}
